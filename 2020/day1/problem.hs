module Problem where

import Data.List (subsequences)

solve list x n = filter (/= []) (map helper (filter (\l -> length l == n) (subsequences list)))
  where
    helper lst
      | sum lst == x = lst
      | otherwise = []