const sample = `16
10
15
5
1
11
7
19
6
12
4`;

function max(a, b) {
    return a > b ? a : b;
}
const arr = sample.split('\n').map(parseInt);
const deviceJoltage = arr.reduce(max) + 3;